package scalatest

import wordwrap._;

object Main {

     def main(args:Array[String]) = {
       val s: String = WordWrapper.wrap("abcdefg", 2)
       println("wrapped string is: " + s)
     }
}