package verylongint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class VeryLongInt {

	private List<Integer> digits;
	
	public VeryLongInt(String n) {
		
		final char ZERO = '0';
		char c;
		if(n == null)
		   throw new IllegalArgumentException("constructor argument cannot be null");
		
		int length = n.length();
		boolean atLeastOneDigit = false;
		digits = new ArrayList<>();
		
		for(int i = 0; i < length; ++i) {
			c = n.charAt(i);
			if(Character.isDigit(n.charAt(i))) {
				atLeastOneDigit = true;
				digits.add(c - ZERO);
			}
		}
		
		if(!atLeastOneDigit)
			throw new IllegalArgumentException("Does not contain any digit");
		
	}
	
	public void add(VeryLongInt other) {
	    
		final int BASE = 10;
		
		int largerSize, partialSum, carry = 0;
		
		largerSize = digits.size() > other.digits.size() ? digits.size() : other.digits.size();
	    
		List<Integer> result = new ArrayList<>();
		
	    for(int i = 0; i < largerSize; ++i) {
	    	 partialSum = this.lastDigit(i) + other.lastDigit(i) + carry;
	    	 carry = partialSum / BASE;
	    	 result.add(partialSum % BASE);
	     }
	    
	     if(carry == 1)
	       result.add(carry);
	     Collections.reverse(result);
	     digits = result;
	}
	
	private int lastDigit(int n) {
		
		if(n >= digits.size())
		   return 0;
		return digits.get(digits.size() - 1 - n);
	}
	
	@Override
	public String toString() {
		return digits.toString();
	}
}
