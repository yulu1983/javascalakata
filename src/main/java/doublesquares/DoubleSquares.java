/**
 * @author Yuan Lu
 * Code eval, Double Squares problem solution
 */
package doublesquares;

import java.io.*;
public class DoubleSquares {

	  public static void main (String[] args) throws IOException {
	        File file = new File(args[0]);
	        BufferedReader buffer = new BufferedReader(new FileReader(file));
	        String line;
	        boolean firstN = false;
	        int N = 0;
	        int index = 0;
	        while ((line = buffer.readLine()) != null) {
	            line = line.trim();
	            if(line.length() > 0) {
	            	if(!firstN) {
	            	   N = Integer.parseInt(line);
	            	   firstN = true;
	            	}
	            	else if(index <= N){
	            		++index;
	            		System.out.println(eval(Integer.valueOf(line)));	
	            	}       	   
	            }
	        }
	        buffer.close();
	    }
	
	public static int eval(int n) {
		
		int sqrt = (int)Math.sqrt((double)n/2);
		int count = 0;
		for(int i = 0; i <=sqrt; ++i) {
			double j = Math.sqrt((double)n - Math.pow(i, 2));
			if(j - (int)j == 0.0)
				count++;
		}
			
		return count;
	}
}
