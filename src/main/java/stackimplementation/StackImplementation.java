package stackimplementation;

import java.io.*;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class StackImplementation{

	private List<Integer> list;
	
	public StackImplementation() {
		list = new ArrayList<Integer>();
	}
	
	public StackImplementation(List<Integer> list) {
		this.list = list;
	}
	
	public void push(Integer i) {
	   list.add(i);	
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	public Integer pop() throws EmptyStackException{
	   int s = list.size();
	   if(s == 0)
		   throw new EmptyStackException();
	   else
		return list.remove(s - 1);
	}
	
	public int size() {
		return list.size();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		Iterator<Integer> it = list.iterator();
		while(it.hasNext()) {
			sb.append(it.next());
			if(it.hasNext())
			   sb.append(", ");
		}
		sb.append("]");
		return sb.toString();
	}

	/*
	 * Should Stack has an iterator ?? 
	@Override
	public Iterator<Integer> iterator() {
		return new StackImplIterator();
	}
	
	private class StackImplIterator implements Iterator<Integer> {

		@Override
		public boolean hasNext() {
			return (list.size() != 0);
		}

		@Override
		public Integer next() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	*/
	
	public static void main (String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        Pattern pattern = Pattern.compile("(-*\\d+)\\s*");
        
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            if(line.length() > 0) {
            	
            	java.util.regex.Matcher matcher = pattern.matcher(line);
            	List<Integer> list = new ArrayList<>();
            	while(matcher.find()) {
            		list.add(Integer.valueOf(matcher.group(1)));
            	}
            	StackImplementation st = new StackImplementation(list);
            	int index = 1;
            	while(!st.isEmpty()) {
            	   if((index & 1) != 0) {
            	      System.out.print(st.pop());
            	      if(st.isEmpty() || st.size() == 1)
            	    	  System.out.println();
            	      else
            	    	  System.out.print(" ");
            	   }
            	   else {
            		  st.pop(); 
            	   }
            	   ++index;
            	}
            }
        }
        buffer.close();
    }
	
}