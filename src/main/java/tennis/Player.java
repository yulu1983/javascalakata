package tennis;



/**
 * We will not use java enum to represent score
 * in the first iteration
 */

public class Player {
   
   private int score;
   private String name;
   
   public Player() {}
   
   public Player(String name) {
	  score = 0;
	  this.name = name;
   }
   
   public void winBall() {
      ++score;	   
   }
   
   public int getScore() {
	   return score;
   }
   
   public String getName() {
	   return name;
   }
   
   public void setName(String name) {
	   this.name = name;
   }
}
