package tennis;


enum GameEvent {
	PLAYER1WONPOINT,
	PLAYER2WONPOINT,
	PLAYER1WONGAME,
	PLAYER2WONGAME,
	PLAYER1WONSET,
	PLAYER2WONSET;
}

public interface IGame {
   String getScore();
   void winBall(GameEvent event);
}
