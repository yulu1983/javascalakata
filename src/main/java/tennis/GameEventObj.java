package tennis;

import java.util.EventObject;

public class GameEventObj extends EventObject{
	
	   private GameEvent event;
	   
	   public GameEventObj(Object source, GameEvent event) {
		   super(source);
		   this.event = event;
	   }
	   
	   public GameEvent getGameEvent() {
		   return event;
	   }

}
