package tennis;

import java.util.Stack;

public class TSet implements GameListener{

	private int player1GamePoints;
	private int player2GamePoints;
	
	private Stack<Game> gameList;
	
	private SetListener listener;
	
	public TSet() {
		gameList = new Stack<>();
		player1GamePoints = 0;
		player2GamePoints = 0;
	}
	
	public void setNewGame(Game newGame) {
		gameList.push(newGame);
	}
	
	public Game getCurrentGame() {
		return gameList.peek();
	}
	
	public void setSetListener(SetListener listener) {
		this.listener = listener;
	}
	
	public String getScore() {
		return player1GamePoints + "-" + player2GamePoints;
	}

	@Override
	public void gameStateChanged(GameEventObj event) {
		GameEvent e = event.getGameEvent();
		switch (e) {
		   case PLAYER1WONGAME: if(isSetWon()) {
			    fireSetStateChanged(GameEvent.PLAYER1WONSET);
		      }
		      ++player1GamePoints;
		      break;
		   case PLAYER2WONGAME: if(isSetWon()) { 
			    fireSetStateChanged(GameEvent.PLAYER2WONSET); break;
		      }
		      ++player2GamePoints;
		      break;
		   default: break;
		}
		
	}
	
	private void fireSetStateChanged(GameEvent e) {
		GameEventObj obj = new GameEventObj(this, e);
		listener.setStateChanged(obj);
	}
	
	private boolean isSetWon() {
		return false;
	}
	
}
