package tennis;

public class Game implements IGame{

	private Player player1;
	private Player player2;
	private GameListener listener;
	
	private String[] scoreArray = {"0", "15", "30", "40"};
	
	public Game(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
		
	}
	
	public void setGameListener(GameListener listener) {
		this.listener = listener;
	}
	
	public String getScore() {
		int p1Score = player1.getScore();
		int p2Score = player2.getScore();
		
		if(isGameWon()) {
			if(p1Score > p2Score)
				return "p1 won";
			else
				return "p2 won";
		}
		
		if(p1Score <= 3 && p2Score < 3)
			return scoreArray[p1Score] + "-" + scoreArray[p2Score];
		else if((p2Score <= 3 && p1Score < 3))
			return scoreArray[p1Score] + "-" + scoreArray[p2Score];
		else if(p1Score >= 3 && p1Score == p2Score)
		   return "duce";
		else if(p1Score > 3 && p1Score > p2Score)
		   return "adv-";
		else if(p1Score > 3 && p1Score < p2Score)
		   return "-adv";
		return "default score";
	}
	
	public void winBall(GameEvent event) {
		switch(event) {
		   case PLAYER1WONPOINT: player1.winBall(); if(isGameWon()) fireGameWonEvent(GameEvent.PLAYER1WONGAME);break;
		   case PLAYER2WONPOINT: player2.winBall(); if(isGameWon()) fireGameWonEvent(GameEvent.PLAYER2WONGAME);break;
		   default: break; // default will do nothing
		}
	}
	
	private void fireGameWonEvent(GameEvent event) {
		GameEventObj obj = new GameEventObj(this, event);
		listener.gameStateChanged(obj);
	}
	
	private boolean isGameWon() {

		int p1Score = player1.getScore();
		int p2Score = player2.getScore();
		
		return ((p1Score > 3 && (p1Score - p2Score > 1)) 
				|| (p2Score > 3 && (p2Score - p1Score > 1)));
		
	}
}
