package tennis;

import java.util.EventListener;

public interface GameListener extends EventListener{

	public void gameStateChanged(GameEventObj event);
}
