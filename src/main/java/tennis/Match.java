package tennis;

import java.util.Stack;

public class Match implements SetListener{
   
   private int player1SetPoints;
   private int player2SetPoints;
   
   private Stack<TSet> tSets;
   
   public Match() {
	   this.player1SetPoints = 0;
	   this.player2SetPoints = 0;
	   this.tSets = new Stack<>();
   }
   
   public String getScore() {
      return player1SetPoints  + "-" + player2SetPoints;
   }
   
   public void setNewSet(TSet tset) {
	   tSets.push(tset);
   }
   
   public TSet getCurrentSet() {
	   return tSets.peek();
   }
   
   @Override
   public void setStateChanged(GameEventObj event) {
       GameEvent e = event.getGameEvent();
       switch (e) {
          case PLAYER1WONSET: ++player1SetPoints; break;
          case PLAYER2WONSET: ++player2SetPoints; break;
          default: break;
       }
   }
	
}

