package tennis;

import java.util.EventListener;

public interface SetListener extends EventListener{

	   public void setStateChanged(GameEventObj event);
}
