/**
 * @author Yuan Lu
 * 
 * Note that the method is specifically for problem at code eval based on the input
 * please pay attention to the index.
 * The last element of the list in the index number to be printed (1-based) from the 
 * last element
 */
package mthelementtothelast;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;

public class MthElement {

	 public static void main (String[] args) throws IOException {
	        File file = new File(args[0]);
	        BufferedReader buffer = new BufferedReader(new FileReader(file));
	        String line;
	        while ((line = buffer.readLine()) != null) {
	            line = line.trim();
	            if(line.length() == 0)
	            	continue;
	            Scanner sc = new Scanner(line);
	            List<String> list = new ArrayList<>();

	            while(sc.hasNext()) {
	            	String c = sc.next();
	            	list.add(c);
	            }
	            sc.close();
	            if(list.size() >= 2) {
	            	String m = list.get(list.size() - 1);
	            	int mi = Integer.valueOf(m);
	            	if(mi <= list.size() - 1)
	            		System.out.println(getMthElement(list, mi));
	            }
	        }
	        buffer.close();
	    }
	
	
	
	 public static String getMthElement(List<String> list, int m) {
	   int size = list.size() - 1;
	   for(int i = 0, j = m - 2; i < size - 1; ++i, ++j) {
		   if(j == size - 2)
			   return list.get(i);  
	   }
	   return null;
   }
   
   
	
}
