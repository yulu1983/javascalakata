/**
* @author Yuan Lu
* 
* This is an in place reverse of words implementation, it also works for 
* a single word situation
*  
*/

package easyreversingwords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReversingWords {
	
	
	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if(line.length() > 0) {
				System.out.println(reverse(line));
			}
		}
		buffer.close();
	}
	
	
	public static String reverse(String word) {
		char[] ca = word.toCharArray();
		reverse(ca);
		return new String(ca);
	}
	
	private static void reverse(char[] ca) {
		reverseASingleWord(ca, 0, ca.length - 1);
		
		int j = 0;
		for(int i = 0; i < ca.length; ++i) {
			if(ca[i] == ' ') {
				reverseASingleWord(ca, j, i - 1);
				j = i + 1;
			}
		}
		reverseASingleWord(ca, j, ca.length - 1);
		
	}
	
	private static void reverseASingleWord(char[] ca, int start, int end) {
		
		for(int i = start, j = end; i <=j; i++, j--) {
		   char tmp = ca[i];
		   ca[i] = ca[j];
		   ca[j] = tmp;
		}	
	}

}
