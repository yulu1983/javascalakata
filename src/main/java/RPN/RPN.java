package RPN;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class RPN {

	public static Double calc(String input) {
		Stack<Double> numbers = new Stack<>();
		
		String[] args = input.split(" ");
		
		for(String arg: args) {
			Sign sign = Sign.find(arg);
			if(sign != null) {
				calcSign(numbers, sign);
			}
			else
				numbers.push(Double.parseDouble(arg));
		}
	
		return numbers.pop();
	}
	
	private static void calcSign(Stack<Double> numbers, Sign sign) {
		numbers.push(sign.apply(numbers.pop(), numbers.pop()));
	}
	
	public enum Sign {
		ADD("+") {
			public double apply(double number1, double number2) {
				return number1 + number2;
			}
		},
		
		MINUS("-") {
			public double apply(double number1, double number2) {
				return number1 - number2;
			}
		},
		
		MULTIPLY("*") {
			public double apply(double number1, double number2) {
				return number1 * number2;
			}
		},
		

		DIVIDE("/") {
			public double apply(double number1, double number2) {
				return number1 / number2;
			}
		};
		
		
		public abstract double apply(double number1, double number2);
		
		private String signText;
		private Sign(String signText) { this.signText = signText; }
		private static Map<String, Sign> signMap;
		
		static {
			signMap = new HashMap<>();
			for(Sign sign : Sign.values()) {
				signMap.put(sign.signText, sign);
			}
		}
		
		public static Sign find(String sign) {
			return signMap.get(sign);
		}
	}

}
