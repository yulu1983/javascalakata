package designpattern.structual.bridge;

public abstract class AbstractDriverControl {
   private Engine engine;
   public AbstractDriverControl(Engine engine) {
	   this.engine = engine;
   }
   
   public void ignitionOn() {
	   engine.start();
   }
   
   public void ignitionOff() {
	   engine.stop();
   }
   
   public void accelerate() {
	   engine.increasePower();
   }
   
   public void brake() {
	   engine.decreasePower();
   }
   
   
}
