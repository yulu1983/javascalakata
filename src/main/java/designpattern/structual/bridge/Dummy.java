package designpattern.structual.bridge;

public class Dummy {

	private boolean running;
	
	public Dummy() {
		running = true;
	}
	
	public boolean isRunning() { return running; }
}
