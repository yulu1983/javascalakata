package designpattern.structual.bridge;

public interface Engine {
   void start();
   void stop();
   void increasePower();
   void decreasePower();
}
