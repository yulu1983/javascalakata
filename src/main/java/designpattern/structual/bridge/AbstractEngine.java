package designpattern.structual.bridge;

public abstract class AbstractEngine implements Engine{
   
	private int size;
    private boolean turbo;
    protected boolean running;
    protected int power;
    
    public AbstractEngine(int size, boolean turbo) {
    	this.size = size;
    	this.turbo = turbo;
    	this.running = false;
    	this.power = 0; 
    }
    
    public int getSize() { return size; }
    public boolean isTurbo() { return turbo; }
    public boolean isRunning() {return running;}
    
    public void start() { running = true; }
    public void stop() { running = false; }
    
    public void increasePower() {
    	power++;
    }
    
    public void decreasePower() {
    	--power;
    }
    
}
