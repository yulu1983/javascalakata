package designpattern.structual.bridge;

public class TurboEngine extends AbstractEngine{

	public TurboEngine(int size, boolean turbo) {
		super(size, turbo);
	}
	
	@Override
	public void increasePower() {
		power++;
		power++;
	}
	
	@Override
	public void decreasePower() {
		--power;
		--power;
	}

}
