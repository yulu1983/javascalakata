package designpattern.creational.abstractfactory;

public class CarBody implements Body{

	@Override
	public String getBodyParts() {
		return "car body parts";
	}

}
