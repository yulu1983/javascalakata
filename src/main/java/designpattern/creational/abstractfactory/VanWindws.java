package designpattern.creational.abstractfactory;

public class VanWindws implements Windows {

	@Override
	public String getWindowsParts() {
		return "van windows parts";
	}

}
