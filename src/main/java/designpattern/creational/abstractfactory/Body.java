package designpattern.creational.abstractfactory;

public interface Body {
   
	public String getBodyParts();
}
