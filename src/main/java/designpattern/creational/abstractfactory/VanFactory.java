package designpattern.creational.abstractfactory;

public class VanFactory extends AbstractViecleFactory {

	@Override
	public Body buildBody() {
		return new VanBody();
	}

	@Override
	public Chassis buildChassis() {
		return new VanChassis();
	}

	@Override
	public Windows buildWindows() {
		return new VanWindws();
	}

}
