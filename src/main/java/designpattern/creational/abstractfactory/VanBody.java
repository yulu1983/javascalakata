package designpattern.creational.abstractfactory;

public class VanBody implements Body {

	@Override
	public String getBodyParts() {
		return "van body parts";
	}

}
