package designpattern.creational.abstractfactory;

public class CarWindows implements Windows {

	@Override
	public String getWindowsParts() {
		return "car windows parts";
	}

}
