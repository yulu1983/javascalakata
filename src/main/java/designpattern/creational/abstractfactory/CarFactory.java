package designpattern.creational.abstractfactory;

public class CarFactory extends AbstractViecleFactory {

	@Override
	public Body buildBody() {
		return new CarBody();
	}

	@Override
	public Chassis buildChassis() {
		return new CarChassis();
	}

	@Override
	public Windows buildWindows() {
		return new CarWindows();
	}

}
