package designpattern.creational.abstractfactory;

public class VanChassis implements Chassis {

	@Override
	public String getChassisParts() {
		return "van chassis parts";
	}

}
