package designpattern.creational.abstractfactory;

public abstract class AbstractViecleFactory {

	public abstract Body buildBody();
	public abstract Chassis buildChassis();
	public abstract Windows buildWindows();
}
