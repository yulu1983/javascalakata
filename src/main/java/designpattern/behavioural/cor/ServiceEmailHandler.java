package designpattern.behavioural.cor;

public class ServiceEmailHandler extends AbstractEmailHandler{

	@Override
	protected String[] matchingWords() {
		return new String[] {"service", "repair"};
	}

	@Override
	protected String handleHere(String email) {
		return "service handler";
	}

}
