package designpattern.behavioural.cor;

public interface EmailHandler {
	
	public void setNextHandler(EmailHandler handler);
	public String processHandler(String mail);

}
