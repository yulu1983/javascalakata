package designpattern.behavioural.cor;

public class ManagerEmailHandler extends AbstractEmailHandler {

	@Override
	protected String[] matchingWords() {
		return new String[] {"complain", "bad"};
	}

	@Override
	protected String handleHere(String email) {
		return "manager handler";
	}

}
