package designpattern.behavioural.cor;

public abstract class AbstractEmailHandler implements EmailHandler{

	private EmailHandler nextHandler;
	
	
	public static String handle(String email) {
		EmailHandler spam = new SpamEmailHandler();
		EmailHandler sales = new SalesEmailHandler();
		EmailHandler service = new ServiceEmailHandler();
		EmailHandler manager = new ManagerEmailHandler();
		EmailHandler general = new GeneralEmailHandler();
		
		spam.setNextHandler(sales);
		sales.setNextHandler(service);
		service.setNextHandler(manager);
		manager.setNextHandler(general);
		
		return spam.processHandler(email);
	}
	
	
	@Override
	public void setNextHandler(EmailHandler handler) {
		this.nextHandler = handler;
		
	}

	@Override
	public String processHandler(String email) {
		boolean wordFound = false;
		
		if(matchingWords().length == 0) {
			wordFound = true;
		}
		
		else {
			for(String word: matchingWords()) {
				if(email.indexOf(word) >= 0) {
					wordFound = true;
					break;
				}
			}
		}
			
		if(wordFound) {
			return handleHere(email);
		}
		else {
			return nextHandler.processHandler(email);
		}

	}
	
	protected abstract String[] matchingWords();
	protected abstract String handleHere(String email);

}
