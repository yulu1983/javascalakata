package designpattern.behavioural.cor;

public class SalesEmailHandler extends AbstractEmailHandler{

	@Override
	protected String[] matchingWords() {
		return new String[] {"buy", "purchase"};
	}

	@Override
	protected String handleHere(String email) {
		return "sales handler";
	}

}
