package designpattern.behavioural.cor;

public class SpamEmailHandler extends AbstractEmailHandler{

	@Override
	protected String[] matchingWords() {
		return new String[] {"viagra", "pills", "medicines"};
		
	}

	@Override
	protected String handleHere(String email) {
		return "spam handler";
	}
}
