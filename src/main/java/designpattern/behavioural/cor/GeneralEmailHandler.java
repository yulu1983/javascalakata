package designpattern.behavioural.cor;

public class GeneralEmailHandler extends AbstractEmailHandler {

	@Override
	protected String[] matchingWords() {
		return new String[0];
	}

	@Override
	protected String handleHere(String email) {
		return "general handler";
	}

}
