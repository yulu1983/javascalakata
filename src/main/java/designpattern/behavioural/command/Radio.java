package designpattern.behavioural.command;

public class Radio {
  
   private static final int MAX_VOLUME = 15;
   private static final int MIN_VOLUME = 0;
   
	
   private int volume;
   
   
   public Radio() {
	   volume = MIN_VOLUME;
   }
   
   public int getVolume() { return volume; }
   
   public void increaseVolume() { 
	  if(volume < MAX_VOLUME) ++volume;
   }
   
   public void decreaseVolume() {
	   if(volume > MIN_VOLUME) --volume;
   }
}
