package designpattern.behavioural.command;

public class SpeechRecogniser {

    private Command upCommand;
    private Command downCommand;
	
	public SpeechRecogniser(Command upCommand, Command downCommand) {
		this.upCommand = upCommand;
		this.downCommand = downCommand;
	}
	
	public void actOnHearingUp() {
		upCommand.execute();
	}
	
	public void actOnHearingDown() {
		downCommand.execute();
	}
}
