package readmore;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadMore {

	private final static int MAXLENGTH = 55;
	
	// counting from 0
	private final static int MAXINDEX = 39;
	private final static String SUFFIX = "... <Read More>";
	
	public static String trim(String s) {
		if(s.length() <= MAXLENGTH)
			return s.trim();
		else {
			if(s.charAt(MAXINDEX + 1) == ' ') {
				return s.substring(0, MAXINDEX + 1) + SUFFIX;
			}
			else {
				int space = s.substring(0, MAXINDEX + 1).lastIndexOf(' ', MAXINDEX);
				if(space == -1)
					return s.substring(0, MAXINDEX + 1) + SUFFIX;
				return s.substring(0, space) + SUFFIX;
			}

		}
	}
	
	public static void main(String...args) throws Exception {
		Scanner sc = new Scanner(new File(args[0]));
		while(sc.hasNext()) {
			String line = sc.nextLine();
			if(line.trim().length() > 300)
				continue;
			if(line.length() > 0)
			   System.out.println(trim(line));
		}
		sc.close();
	}
}
