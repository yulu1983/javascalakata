package longestlines;

/* Sample code to read in test cases: */
import java.io.*;
import java.util.Comparator;
import java.util.PriorityQueue;
public class LongestLines {
    public static void main (String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        line = buffer.readLine();
        line = line.trim();
        Integer n = Integer.valueOf(line);
        
        
        PriorityQueue<String> pq = new PriorityQueue<>(100, new LongestLines().new ByLength());
        
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            pq.add(line);
        }
        buffer.close();
        
        for(int i = 0; i < n; ++i)
        	System.out.println(pq.remove());
    }
    
    private class ByLength implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			if(o1.length() > o2.length())
				return -1;
			else if (o1.length() < o2.length())
				return 1;
			else
				return 0;
		}
    }
}
