package mergesort2;

public class MergeSort2 {

	private static final int THRESHOULD = 7;
	
	public static void sort(Object[] a) {
		// create an aux array, dup a
		Object[] aux = (Object[]) a.clone();
		mergeSort(aux, a, 0, aux.length);
	}
	
	private static void mergeSort(Object[] src, Object[] dest, int low, int high) {
		int length = high - low; 
		if(length <= THRESHOULD) {
			for(int i = low; i < high; ++i)
				for(int j = i; j > low; --j)
					if(((Comparable)dest[j - 1]).compareTo(dest[j]) > 0)
						swap(dest, j - 1, j);
			return;
		}
		
		int mid = (high + low) >> 1;
		mergeSort(dest, src, low, mid);
		mergeSort(dest, src, mid, high);
		

		if(((Comparable) src[mid - 1]).compareTo(src[mid]) <= 0) {
			System.arraycopy(src, low, dest, low, high);
			return;
		}
		
		for(int i = low, p = low, q = mid; i < high; ++i) {
			if(q >= high || ( p < mid && ((Comparable)src[p]).compareTo(src[q]) <= 0))
					dest[i] = src[p++];
			else
				dest[i] = src[q++];
		}
	}
	
	private static void swap(Object[] arr, int a, int b) {
		Object tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}
}
