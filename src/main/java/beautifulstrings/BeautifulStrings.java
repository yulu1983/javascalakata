/**
 * @author Yuan Lu
 * 
 * Sample solution for code eval beautiful strings
 */
package beautifulstrings;

import java.util.Arrays;
import java.io.*;

public class BeautifulStrings {
	
	public static void main (String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            if(line.length() > 0) {
            	System.out.println(eval(line));
            }
        }
        buffer.close();
    }
	
	public static int eval(String s) {
		
	    int[] array = new int[26];
		
		char[] carray = s.toLowerCase().toCharArray();
		
		for(char c : carray) {
			if(Character.isLetter(c))
				array[c - 'a'] += 1; 
		}
		
		Arrays.sort(array);
		int total = 0;
		for(int i = 25; i >= 0; --i) {
		   if(array[i] == 0)
			   break;
		   else
		      total += (i + 1) * array[i]; 
		}
		
		return total;
	}

}
