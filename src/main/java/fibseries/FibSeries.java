/**
 * @author Yuan Lu
 * A tail recursive version of fib sequence
 */

package fibseries;

import java.io.*;

public class FibSeries {

	 public static void main (String[] args) throws IOException {
	        File file = new File(args[0]);
	        BufferedReader buffer = new BufferedReader(new FileReader(file));
	        String line;
	        while ((line = buffer.readLine()) != null) {
	            line = line.trim();
	            System.out.println(FibSeries.get(Integer.valueOf(line)));
	        }
	        buffer.close();
	    }
	
	
	public static int get(int n) {
	   	if(n == 0)
	   		return 0;
	   	else if(n == 1)
	   		return 1;
	   	else
		   return helper(0, 1, n);
	}
	
	private static int helper(int f1, int f2, int n) {
		if(n == 2)
			return f1 + f2;
		else
			return helper(f2, f1 + f2, --n);			
	}
	
	
}
