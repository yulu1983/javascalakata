/**
 * @author Yuan Lu
 * String Permuation solution for code eval
 */
package stringpermutation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.*;

public class StringPermutation {

	public static void main (String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            if(line.length() > 0) {
            	List<String> list = permutate(line);
            	// since the complier on code eval is 1.7
            	// we cannot use StringJoiner class
            	for(int i = 0; i < list.size(); ++i) {
            		if(i == list.size() - 1)
            			System.out.println(list.get(i));
            		else
            			System.out.print(list.get(i) + ",");
            	}
            }
        }
        buffer.close();
    }
	
	public static List<String> permutate(String s) {
       List<String> list = new ArrayList<>();
       permutate("", s, list);
       Collections.sort(list);
       return list;
	}
	
	private static void permutate(String prefix, String s, List<String> list) {
		if(s.length() == 0) {
			list.add(prefix);
		}
		else {
			for(int i = 0; i < s.length(); ++i) {
			   	char ch = s.charAt(i);
			   	String rest = s.substring(0, i) + s.substring(i + 1);
			   	permutate(prefix + ch, rest, list);
			}

		}
	}

}


