/**
 * @author Yuan Lu
 * 
 * An Implementation of solving Longgest common subsequence problem, sequence are not needed to be consecutive.
 */
package LCS;

import java.io.*;

public class LCS {

	public enum ARROW {
		DIAGONAL, LEFT, UPPER, NONE;
	}
	
	public static class Node {
		
		public Node() {
			arrow = ARROW.NONE;
			value = 0;
		}
		
		ARROW arrow;
		int value;
	}
	
	public static String find(String s1, String s2) {
		int m = s1.length() + 1;
		int n = s2.length() + 1;
		
		Node[][] v = new Node[m][n];
		
		for(int i = 0; i < m; ++i)
			for(int j = 0; j < n; ++j) {
				v[i][j] = new Node();
		}
		
		for(int i = 1; i < n; ++i)
			for(int j = 1; j < m; ++j) {
				if(s1.charAt(j - 1) == s2.charAt(i - 1)) {
					v[j][i].value = v[j - 1][i - 1].value + 1;
					v[j][i].arrow = ARROW.DIAGONAL;
					// update the map
				}
				else {
					
					if(v[j - 1][i].value >= v[j][i - 1].value)
						v[j][i].arrow = ARROW.LEFT;
					else
						v[j][i].arrow = ARROW.UPPER;
					
					v[j][i].value = Math.max(v[j - 1][i].value, v[j][i - 1].value);
				}
			}
		
		Node node = v[m - 1][n - 1];
		int index = v[m - 1][n - 1].value - 1;
		
		char[] sequence = new char[v[m - 1][n - 1].value];
		int a = m - 1;
		int b = n - 1;
		while(node.value != 0 && node.arrow != ARROW.NONE)
		{
			if(node.arrow == ARROW.DIAGONAL) {
				sequence[index--] = s2.charAt(b - 1);
				node = v[--a][--b];
			}
			else if(node.arrow == ARROW.LEFT) {
				node = v[--a][b];
			}
			else if(node.arrow == ARROW.UPPER) {
				node = v[a][--b];
			}
		}
		return new String(sequence);	
	}
	
	public static void main(String[] args) throws IOException{
		File file = new File(args[0]);
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            if(line.length() > 0) {
            	String[] s = line.split(";");
            	System.out.println(find(s[0], s[1]));
            }
        }
	}

}
