package wordwrap;

/**
 * A Tail recursive version of Word Wrap
 * Easy to translate into a iterative solution using loop
 * 
 * @authror Yuan Lu
 */

public class WordWrapper {
   
   public static String wrap(String s, int col) {
	   if(s != null && s.length() <= col)
		   return s;
	   else {
		   StringBuilder b = new StringBuilder();
		   return helper(b, s, col);
	   }
		   
   }

   private static String helper(StringBuilder b, String rest, int col) {
	   if(rest.length() <= col)
		   return b.append(rest).toString();
	   else {
		   int space = rest.substring(0, col).lastIndexOf(' ');
		   if(space != -1) 
		      return helper(b.append(rest.substring(0, space) + "\n"), rest.substring(space + 1), col); 
		   else if(rest.charAt(col) == ' ')
			   return helper(b.append(rest.substring(0, col) + "\n"), rest.substring(col + 1), col);
		   else
			   return helper(b.append(rest.substring(0, col) + "\n"), rest.substring(col), col);
	   }	  
   }
   
   public static String wrapIterative(String s, int col) {
	   if(s != null && s.length() <= col)
		   return s;
	   else {
		   StringBuilder b = new StringBuilder();
		   String rest = s;
		   while(rest.length() > col) {
			   int space = rest.substring(0, col).lastIndexOf(' ');
			   if(space != -1) {
				   b.append(rest.substring(0, space) + "\n");
			       rest = rest.substring(space + 1);
			   }
			   else if(rest.charAt(col) == ' ') {
				   b.append(rest.substring(0, col) + "\n");
			       rest = rest.substring(col + 1);
			   }
			   else {
				   b.append(rest.substring(0, col) + "\n");
				   rest = rest.substring(col);
			   }   
		   }
		   return b.append(rest).toString();
	   }
   }
   
   
}
