package treemap;

import java.util.List;

public interface Thesaurus {

	/**
	 * Adds a specified line of synonyms to this Thesaurus object.
	 * The worstTime is O(log n).
	 * 
	 * @param line - the specified line of synonyms to be added to this
	 *        Thesaurus object.
	 * @throws NullPointerException - if line is null.
	 *        
	 */
	public void add(String line);
	
	/**
	 * Returns a Strin
	 * @return
	 */
	public List<String> getSynonyms(String word);
	
	
	/**
	 * Returns a String representation of this Thesaurus object.
	 * The worstTime is O(n)
	 * 
	 * @return a String representation of this Thesaurus object in the
	 *         form {word1=[syn11, syn12, ...],  word2=[syn21, syn22,...],...}
	 */
	@Override
	public String toString();
}
