package treemap;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class ThesaurusImpl implements Thesaurus {

	protected Map<String, List<String>> thesaurusMap;
	
	public ThesaurusImpl() {
		thesaurusMap = new TreeMap<>();
	}
	
	@Override
	public void add(String line) {
       List<String> synonymList = new LinkedList<>();
       Scanner sc = new Scanner(line);
       
       if(sc.hasNext()) {
    	   String word = sc.next();
    	   
    	   while(sc.hasNext())
    		   synonymList.add(sc.next());
    	   
    	   thesaurusMap.put(word, synonymList);
       }
	}

	@Override
	public List<String> getSynonyms(String word) {
		return thesaurusMap.get(word);
	}
	
	@Override
	public String toString() {
		return thesaurusMap.toString();
	}

}
