package mergesort;

public class MergeSort {
	
	public static void sort(Object[] a) {
		Object[] aux = (Object[]) a.clone();
		mergeSort(aux, a, 0, a.length);
	}
	
	private static void mergeSort(Object[] src, Object[] dest, int low, int high) {
		int length = high - low;
		
		if(length <= 7) {
			for(int i = low; i < high; ++i) {
				for(int j = i; j > low; --j) {
					if(((Comparable) dest[j - 1]).compareTo(dest[j]) > 0)
						swap(dest, j, j - 1);
				}
			}
			return;
		}
		
		// if not insertion sort then further split the array
		// This could potentially result in overflow
		int mid = (low + high) >> 1;
		mergeSort(dest, src, low, mid);
		mergeSort(dest, src, mid, high);
		
		if(((Comparable) src[mid - 1]).compareTo(src[mid]) <= 0) {
			System.arraycopy(src, low, dest, low, high);
			return;
		}
		
		for(int i = low, p = low, q = mid; i < high; ++i) {
			if(q >= high || ( p < mid && ((Comparable)src[p]).compareTo(src[q]) <= 0))
					dest[i] = src[p++];
			else
				dest[i] = src[q++];
		}
		
	}
	
	private static void swap(Object[] dest, int a, int b) {
		Object tmp = dest[a];
		dest[a] = dest[b];
		dest[b] = tmp;
	}

}
