package marsrover;

public class Rover {

	public enum Direction {
		NORTH, EAST, SOUTH, WEST
	}
	
	private Point p;
	private Direction d;
	
	public Rover(Point p, Direction d) {
		this.p = p;
		this.d = d;
	}
	
	public int getX() {
		return p.getX();
	}
	
	public int getY() {
		return p.getY();
	}
	
	public Direction getDirection() {
		return d;
	}
	
	public boolean receiveCommand(String c) {
		char[] cArray = c.toUpperCase().toCharArray();
		for(char command : cArray) {
			switch(command) {
			   case 'F': moveForward(); break;
			   case 'B': moveBackward(); break;
			   case 'L': turnLeft(); break;
			   case 'R': turnRight(); break;
			   default: return false;
			}
		}
		return true;
	}
	
	private boolean moveForward() {
	   return move(d);	
	}
	
	private boolean moveBackward() {
	    int size = Direction.values().length;
		return move(Direction.values()[(d.ordinal() + 2) % size]);
	}
	
	private boolean turnLeft() {
		int size = Direction.values().length;
		if(d.ordinal() - 1 < 0)
		   d = Direction.values()[(size -1) % size];
		else
	       d = Direction.values()[(d.ordinal() - 1) % size];
	    return true;
	}
	
	private boolean turnRight() {
		int size = Direction.values().length;
	    d = Direction.values()[(d.ordinal() + 1) % size];
	    return true;
	}
	
	private boolean move(Direction d) {
		switch(d) {
			case NORTH: p.setY(p.getY() + 1); return true;
			case SOUTH: p.setY(p.getY() - 1); return true;
			case EAST: p.setX(p.getX() + 1); return true;
			case WEST: p.setX(p.getX() - 1); return true;
			default: return false;
		}
	}
}
