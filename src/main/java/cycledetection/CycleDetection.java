package cycledetection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CycleDetection {

	public static String showCircle(List<Integer> list) {
	   int[] array = new int[100];
	   
	   for(Integer i : list) {
		   (array[i])++;
	   }
	   
	   int mu = 0;
	   int size = list.size();
	   
	   for(int j = 0; j < size; ++j) {
		   if(array[list.get(j)] > 1) {
			   mu = j;
			   break;
		   }   
	   }
	   
	   int k = mu;
	   
	   for(k = mu; k < size; ++k) {
		   if(list.get(mu) == list.get(k + 1))
			   break;
	   }
	  
	   StringBuilder sb = new StringBuilder();
	   int lamda = k + 1;
	   for(int i = mu; i < lamda; ++i) {
		   sb.append(list.get(i));
		   sb.append(" ");
	   }
	   return sb.toString().trim();
	
	}
	
	public static void main(String[] args) throws IOException{
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			Scanner sc = new Scanner(line);
			List<Integer> list = new ArrayList<>();
			while(sc.hasNextInt()) {
				list.add(sc.nextInt());
			}
			sc.close();
			System.out.println(CycleDetection.showCircle(list));
		}
		buffer.close();
	}
	
	/*
	public static String showCircle(List<String> list) {
		int i = 1;
		int j = i + 1;
		String tortoise = list.get(i);
		String hare = list.get(j);
		while(!tortoise.equals(hare)) {
			i += 1;
			j += 2;
			tortoise = list.get(i);
			hare = list.get(j);
		}
		
		int mu = 0;
		i = 0;
		tortoise = list.get(i);
		while(!tortoise.equals(hare)) {
			i += 1;
			j += 1;
			tortoise = list.get(i);
			hare = list.get(j);
			mu++;
		}
		
		System.out.println("mu is: " + mu);
		
		int lamda = 1;
		i += 1;
		hare = list.get(i);
		
		while(!tortoise.equals(hare)) {
			i += 1;
			hare = list.get(i);
			lamda++;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int k = mu; k < mu + lamda; ++k) {
			sb.append(list.get(k));
			sb.append(" ");
		}
		return sb.toString().trim();
	}
	*/
}
