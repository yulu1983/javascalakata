/**
 * @author Yuan Lu
 * This is code represents my solution to code eval prefix expression question
 */
package prefixexpression;

import java.io.*;
import java.util.Stack;

public class PrefixExpression {

	public static void main(String[] args) throws IOException {
		 File file = new File(args[0]);
	     BufferedReader buffer = new BufferedReader(new FileReader(file));
	     String line;
	     while ((line = buffer.readLine()) != null) {
	    	    line = line.trim();
	            if(line.length() > 0) {
	            	int i = eval(line);
	            	System.out.println(i);
	            }
	     }
	     buffer.close();
	}
	
	public static int eval(String s) {
		
		String[] a = s.split("\\s");
		
		Stack<Double> stack = new Stack<>();
		
		int l = a.length;
		
		for(int i = l - 1; i >=0; --i) {
			switch(a[i]) {
			   case "*": stack.push(Double.valueOf(stack.pop()) * Double.valueOf(stack.pop())); break;
			   case "/": stack.push(Double.valueOf(stack.pop()) / Double.valueOf(stack.pop())); break;
			   case "+": stack.push(Double.valueOf(stack.pop()) + Double.valueOf(stack.pop())); break;
			   default: stack.push(Double.valueOf(a[i]));
			}
		}
		
		return stack.peek().intValue();
	}
	
}
