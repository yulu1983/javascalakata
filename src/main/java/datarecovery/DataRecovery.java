package datarecovery;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class DataRecovery {
	
	public static void main(String...args) throws IOException {
		File file = new File(args[0]);
		BufferedReader buffer = new BufferedReader(new FileReader(file));
		String line;
		while ((line = buffer.readLine()) != null) {
			line = line.trim();
			if(!line.isEmpty())
			   System.out.println(recovery(line));
		}
		buffer.close();
	}
	
	public static String recovery(String s) {
		String[] parts = s.split(";");
		
		String[] wordPart = parts[0].split("\\s");
		String[] indexPart = parts[1].split("\\s");
		
		Map<String, String> map = new HashMap<>();
		
		for(int i = 0; i < indexPart.length; ++i) {
			map.put(indexPart[i], wordPart[i]);
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < wordPart.length; ++i) {
			String value = map.get("" + (i + 1));
			if(value == null)
				sb.append(wordPart[wordPart.length - 1]);
			else
				sb.append(value);
			if(i != wordPart.length - 1)
				sb.append(" ");
			
		}
		
		return sb.toString();
	}
}
