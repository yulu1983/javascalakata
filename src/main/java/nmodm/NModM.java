package nmodm;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Yuan Lu
 * N Mod M without using % operator from codeeval
 *
 */
public class NModM {

	/**
	 * 
	 * @param n positive integer
	 * @param m positive integer
	 * @return n % m
	 */
	public static int mod(int n, int m) {
		if(n == m) return 0;
		else if(n < m) return n;
		else
			return mod(n - m, m);
	}
	
	 public static void main (String[] args) throws IOException {
		 File file = new File(args[0]);
		 BufferedReader buffer = new BufferedReader(new FileReader(file));
		 String line;
		 Pattern pattern = Pattern.compile("(\\d+),(\\d+)");
		 while ((line = buffer.readLine()) != null) {
			 line = line.trim();
			 if(!line.isEmpty()) {
				 Matcher m = pattern.matcher(line);
				 if(m.matches()) {
					 int n = Integer.parseInt(m.group(1));
					 int k = Integer.parseInt(m.group(2));
					 System.out.println(mod(n, k));
				 }
			 }
		 }
		 buffer.close();
	 }
}
