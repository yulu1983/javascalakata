package normalquicksort;

public class NormalQuickSort {

	public static void sort(int[] x) {
		sort(x, 0, x.length);
	}
	
	public static void sort(int[] x, int off, int len) {
	   
		int m = off + (len >> 1), /* divide by 2*/
			n = off + len - 1;	
		
		int median = median(x, off, m, n);
		
		int v = x[median];
		
		int b = off,
			c = off + len - 1;
		
		while(true) {
			while(b <= c && x[b] < v)
				++b;
			while(c >= b && x[c] > v)
				--c;
			
			if(b > c)
				break;
			swap(x, b++, c--);
		}
		
		if(c + 1 - off > 1)
			sort(x, off, c + 1 - off);
		if(off + len - b > 1)
			sort(x, b, off + len - b);
	}
	
	
	public static int median(int x[], int a, int b, int c) {
		if(x[a] < x[b]) {
			if(x[b] < x[c])
				return b;
			else if(x[a] < x[c])
				return c;
			else
				return a;
		}
		
		else {
			if(x[b] > x[c]) 
				return b;
			else if(x[c] > x[a])
				return a;
			else
				return c;
		}
	}
	
	public static void swap(int[] x, int a, int b) {
		int tmp = x[a];
		x[a] = x[b];
		x[b] = tmp;
	}
}

