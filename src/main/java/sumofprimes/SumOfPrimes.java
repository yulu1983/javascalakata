/**
 * @author Yuan Lu
 * The first attempt was to implement Fermat probility primality test
 * However, that implementation requires BigInteger and requires serval interations
 * to confirm the primality.
 * Therefore it violates 20MB of memory usage on codeeval.
 * The following implementation is faster  
 */
package sumofprimes;


import java.io.*;

public class SumOfPrimes {
	
	public static void main(String...args) throws IOException{
		
		/* we precalculate the first 10 prime number */
		
		int count = 10;
		int total = 129;
		int i = 31;
		while(count <= 999) {
			if(isPrime(i)) {
				total += i;
				++count;
			}
			++i;
		}
		System.out.println(total);
    }
	
	public static boolean isPrime(int n) {
		/* if n is even */
		if( (n & 1) == 0) return false;
		
		int i = 5;
	    int w = 2;
		while(i * i <= n) {
		   if (n % i == 0)
			  return false;

		   i += 2;
		}
		return true;
	}
}
