package fibseries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class FibSeriesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void fibTest1() {
		int i = FibSeries.get(5);
		assertThat(i, is(5));
	}
	
	@Test
	public void fibTest2() {
		int i = FibSeries.get(12);
		assertThat(i, is(144));
	}

}
