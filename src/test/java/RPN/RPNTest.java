package RPN;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.hamcrest.Matchers.*;


public class RPNTest {

	@Test
	public void calcShouldBeAbleToCalculateSingleDigitNumber() {
		assertThat(RPN.calc("1 2 +"), is(3.0));
	}
	
	@Test
	public void calcShouldBeAbleToCalculateDoubleDigitNumber() {
		assertThat(RPN.calc("12 5 -"), is(-7.0));
	}
	
	@Test
	public void calcShouldBeAbleToCalculateNegativeNumber() {
		assertThat(RPN.calc("-12 5 *"), is(-60.0));
	}
	
	@Test
	public void calcShouldBeAbleToCalculateALongRPNExpression() {
		assertThat(RPN.calc("3 4 + 5 *"), is(35.0));
	}

}
