package doublesquares;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class DoubleSquaresTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void doubleSquaresTest1() {
	   int n = DoubleSquares.eval(10);
	   assertThat(n, is(equalTo(1)));
	}
	
	@Test
	public void doubleSquaresTest2() {
	   int n = DoubleSquares.eval(25);
	   assertThat(n, is(equalTo(2)));
	}
	
	@Test
	public void doubleSquaresTest3() {
		int n = DoubleSquares.eval(3);
		assertThat(n, is(equalTo(0)));
	}
	
	@Test
	public void doubleSquaresTest4() {
		int n = DoubleSquares.eval(0);
		assertThat(n, is(equalTo(1)));
	}
	
	@Test
	public void doubleSquaresTest5() {
		int n = DoubleSquares.eval(1);
		assertThat(n, is(equalTo(1)));
	}
	
	

}
