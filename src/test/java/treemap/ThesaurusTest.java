package treemap;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class ThesaurusTest {

	private Thesaurus thesaurus;
	
	@Before
	public void setUp() {
		thesaurus = new ThesaurusImpl();
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test(expected = NullPointerException.class)
	public void whenNullIsAddedAddShouldThrowNullPointerException() {
		thesaurus.add(null);
	}
	
	@Test
	public void addShouldAddSynonyms() {
		thesaurus.add("therefore since because ergo");
		assertThat(thesaurus.toString(), is("{therefore=[since, because, ergo]}"));
	}

}
