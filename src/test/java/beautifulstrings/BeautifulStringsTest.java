package beautifulstrings;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class BeautifulStringsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void shouldProduceMaxBeautyValue1() {
		int value = BeautifulStrings.eval("ABbCcc");
		assertThat(value, is(152));
	}
	
	@Test
	public void shouldProduceMaxBeautyValue2() {
		int value = BeautifulStrings.eval("Good luck in the Facebook Hacker Cup this year!");
		assertThat(value, is(754));
	}
	
	@Test
	public void shouldProduceMaxBeautyValue3() {
		int value = BeautifulStrings.eval("Ignore punctuation, please :)");
		assertThat(value, is(491));
	}
	
	@Test
	public void shouldProduceMaxBeautyValue4() {
		int value = BeautifulStrings.eval("Sometimes test cases are hard to make up.");
		assertThat(value, is(729));
	}
	
	@Test
	public void shouldProduceMaxBeautyValue5() {
		int value = BeautifulStrings.eval("So I just go consult Professor Dalves");
		assertThat(value, is(646));
	}
	
	

}
