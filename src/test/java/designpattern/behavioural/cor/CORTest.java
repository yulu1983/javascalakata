package designpattern.behavioural.cor;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class CORTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenEmailContainRepairServiceHandlerShouldBeReturned() {
		String email = "repair";
		
		assertThat(AbstractEmailHandler.handle(email), is(equalTo("service handler")));
	}
	
	@Test
	public void whenEmailContainViagraServiceHandlerShouldBeReturned() {
		String  email = "viagra";
		
		assertThat(AbstractEmailHandler.handle(email), is(equalTo("spam handler")));
	}

}
