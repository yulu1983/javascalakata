package designpattern.behavioural.command;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class CommandPatternTest {

	private Radio radio;
	private Command vUpCommand;
	private Command vDownCommand;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	   radio = new Radio();
	   vUpCommand = new VolumeUpCommand(radio);
	   vDownCommand = new VolumeDownCommand(radio);
	}

	@Test
	public void whenSpeechRecogniserHearVolumeUpVolumeShouldIncreaseBy1() {
		
		SpeechRecogniser sp = new SpeechRecogniser(vUpCommand, vDownCommand);
		sp.actOnHearingUp();
		assertThat(radio, hasProperty("volume", is(equalTo(1))));
		
	}
	
	@Test
	public void whenSpeechRecogniserHearVolumeDownVolumeShouldDecreaseBy1() {
		
		SpeechRecogniser sp = new SpeechRecogniser(vUpCommand, vDownCommand);
		sp.actOnHearingUp();
		sp.actOnHearingUp();
		sp.actOnHearingDown();
		assertThat(radio, hasProperty("volume", is(equalTo(1))));
	}
	
}
