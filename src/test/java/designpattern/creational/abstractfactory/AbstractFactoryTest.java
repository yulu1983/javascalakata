/**
 * 
 */
package designpattern.creational.abstractfactory;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

/**
 * @author t817228
 *
 */
public class AbstractFactoryTest {

	private AbstractViecleFactory factory;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	   factory = null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenCarFactoryProvidedBodyBuildShouldBuildCarBodyParts() {
		factory = new CarFactory();
		assertThat(factory.buildBody().getBodyParts(), is(equalTo("car body parts")));
	}
	
	@Test
	public void whenCarFactoryProvidedChassisBuildShouldBuildCarChassisParts() {
		factory = new CarFactory();
		assertThat(factory.buildChassis().getChassisParts(), is(equalTo("car chassis parts")));
	}
	
	@Test
	public void whenCarFactoryProvidedWindowBuildShouldBuildCarWindowsParts() {
		factory = new CarFactory();
		assertThat(factory.buildWindows().getWindowsParts(), is(equalTo("car windows parts")));
	}
	
	@Test
	public void whenVanFactoryProvidedBodyBuildShouldBuildVanBodyParts() {
		factory = new VanFactory();
		assertThat(factory.buildBody().getBodyParts(), is(equalTo("van body parts")));
	}
	
	
	@Test
	public void whenVanFactoryProvidedChassisBuildShouldBuildVanChassisParts() {
		factory = new VanFactory();
		assertThat(factory.buildChassis().getChassisParts(), is(equalTo("van chassis parts")));
	}
	
	@Test
	public void whenVanFactoryProvidedWindowsBuildShouldBuildVanWindowsParts() {
		factory = new VanFactory();
		assertThat(factory.buildWindows().getWindowsParts(), is(equalTo("van windows parts")));
	}
	

}
