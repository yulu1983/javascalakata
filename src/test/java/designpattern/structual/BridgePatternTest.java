package designpattern.structual;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import designpattern.structual.bridge.AbstractDriverControl;
import designpattern.structual.bridge.Engine;
import designpattern.structual.bridge.StandardDriverControl;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

public class BridgePatternTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenDriverIgnitionOnEngineShouldStart() {
		
		Engine engine = mock(Engine.class);
		AbstractDriverControl control = new StandardDriverControl(engine);
		control.ignitionOn();
		
		verify(engine).start();
		
	}
	
	@Test
	public void whenDriverIgnitionOffEngineShouldSop() {
		
		Engine engine = mock(Engine.class);
		AbstractDriverControl control = new StandardDriverControl(engine);
		control.ignitionOff();
		
		verify(engine).stop();
		
	}
	
	@Test
	public void whenDriverAccelerateEngineShouldIncreasePower() {
		
		Engine engine = mock(Engine.class);
		AbstractDriverControl control = new StandardDriverControl(engine);
		control.accelerate();
		
		verify(engine).increasePower();
		
	}
	
	@Test
	public void whenDriverBreakEngineShouldDecreasePower() {
		
		Engine engine = mock(Engine.class);
		AbstractDriverControl control = new StandardDriverControl(engine);
		control.brake();
		
		verify(engine).decreasePower();
		
	}
	
	

}
