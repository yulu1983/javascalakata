package tennis;

import static org.junit.Assert.*;

import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class TennisMatchTest {

	private Player p1;
	private Player p2;
	private Game game1;
	private TSet tset1;
	private Match match;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		p1 = new Player("p1");
		p2 = new Player("p2");
		game1 = new Game(p1, p2);
		tset1 = new TSet();
		tset1.setNewGame(game1);
		game1.setGameListener(tset1);
		match = new Match();
		match.setNewSet(tset1);
		tset1.setSetListener(match);
	}
	
	@Test
	public void whenPlayer1ScoreGameScoreShouldBeFifteenZero() {
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(game1, hasProperty("score", is(equalTo("15-0"))));
	}
	
	@Test
	public void whenPlayer1ScoreTwiceGameScoreShouldBeThirtyZero() {
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(game1, hasProperty("score", is(equalTo("30-0"))));
	}
	
	@Test
	public void whenPlayer1ScoreThreeTimesGameScoreShouldBeFortyZero() {
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(game1, hasProperty("score", is(equalTo("40-0"))));
	}
	
	@Test
	public void whenMoreThan3ScoredByEachPlayerGameScoreShouldBeDudce() {
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER1WONPOINT));
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER2WONPOINT));
		assertThat(game1, hasProperty("score", is(equalTo("duce"))));
	}
	
	@Test
	public void whenPlayer1ScoredOnceAfterDuceGameScoreShouldBeAdv() {
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER1WONPOINT));
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER2WONPOINT));
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(game1, hasProperty("score", is(equalTo("adv-"))));
	}
	
	@Test
	public void whenPlayer1ScoredTwiceAfterDuceGameScoreShouldBeWonByPlayer1() {
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER1WONPOINT));
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER2WONPOINT));
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(game1, hasProperty("score", is(equalTo("p1 won"))));
	}
	
	@Test
	public void whenPlayer1WonTheSetSetScoreShouldBeIncremented() {
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER1WONPOINT));
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER2WONPOINT));
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(tset1, hasProperty("score", is(equalTo("1-0"))));
	}
	
	@Test
	public void whenPlayer1WonTheFirstGameSetScoreShouldBeZeroZero() {
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER1WONPOINT));
		IntStream.rangeClosed(1, 3).forEach(i -> game1.winBall(GameEvent.PLAYER2WONPOINT));
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		game1.winBall(GameEvent.PLAYER1WONPOINT);
		assertThat(match, hasProperty("score", is(equalTo("0-0"))));
	}
	

	
	
}
