package stringpermutation;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class StringPermutationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void stringPermutationTest1() {
		List<String> list = StringPermutation.permutate("hat");
		assertThat(list.toString(), is(equalTo("[aht, ath, hat, hta, tah, tha]")));
		
	}
	
	@Test
	public void stringPermutationTest2() {
		List<String> list = StringPermutation.permutate("abc");
		assertThat(list.toString(), is(equalTo("[abc, acb, bac, bca, cab, cba]")));
		
	}
	
	@Test
	public void stringPermutationTest3() {
		List<String> list = StringPermutation.permutate("Zu6");
		assertThat(list.toString(), is(equalTo("[6Zu, 6uZ, Z6u, Zu6, u6Z, uZ6]")));
		
	}

}
