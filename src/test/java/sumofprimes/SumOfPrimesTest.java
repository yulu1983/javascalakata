package sumofprimes;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class SumOfPrimesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIfANumberIsAPrime() {
		boolean isPrime = SumOfPrimes.isPrime(7927);
		assertTrue(isPrime);
	}

}
