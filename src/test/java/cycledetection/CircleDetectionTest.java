package cycledetection;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class CircleDetectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenThereIsACircleInSequenceCircleDetectionShouldShowTheFirstCycleSequence() {
		
		List<Integer> list = Arrays.asList(2, 0, 6, 3, 1, 6, 3, 1, 6, 3, 1);
			
		String cycle = CycleDetection.showCircle(list);
		
		assertThat(cycle, is("6 3 1"));
	}
	
	@Test
	public void whenThereIsACircleInSequenceCircleDetectionShouldShowTheFirstCycleSequence2() {
		
		List<Integer> list = Arrays.asList(3, 4, 8, 0, 11, 9, 7, 2, 5, 6, 10, 1, 49, 49, 49, 49);
			
		String cycle = CycleDetection.showCircle(list);
		
		assertThat(cycle, is("49"));
	}
	
	@Test
	public void whenThereIsACircleInSequenceCircleDetectionShouldShowTheFirstCycleSequence3() {
		
		List<Integer> list = Arrays.asList(1, 2, 3, 1, 2, 3, 1, 2, 3);
			
		String cycle = CycleDetection.showCircle(list);
		
		assertThat(cycle, is("1 2 3"));
	}
	
	

}
