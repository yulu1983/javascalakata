package normalquicksort;

import static org.junit.Assert.*;

import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NormalQuickSortTest {

	private int[] array = {59, 46, 32, 80 ,46, 55, 50, 43, 44, 81, 12, 95, 17, 80, 75, 33, 40, 61, 16 ,87};
	
	@Before
	public void setUp() throws Exception {
	   
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void medianFunctionReturnTheCorrectMedian() {
		
		int off = 0;
		int middle = 0 + array.length/2;
		int last = 0 + array.length - 1;
		
		int median = NormalQuickSort.median(array, off, middle,  last);
		
		assertEquals(array[median], 59);
	}
	
	@Test
	public void partitionTest() {
		
	}
	
	@Test
	public void sortTest() {
		NormalQuickSort.sort(array);
		int[] x = {12, 16, 17, 32, 33, 40, 43, 44, 46, 46, 50, 55, 59, 61, 75, 80, 80, 81, 87, 95};
		IntStream.rangeClosed(0, array.length - 1).forEach(i -> assertEquals(array[i], x[i])); 
	}

}
