package mthelementtothelast;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class MthElementTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void shouldReturn4thElementFromLast() {
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("4");
		String element = MthElement.getMthElement(list, 4);
		assertThat(element, is("a"));
	}
	
	@Test
	public void shouldReturn2ndElementFromLast() {
		List<String> list = new ArrayList<>();
		list.add("e");
		list.add("f");
		list.add("g");
		list.add("h");
		list.add("2");
		String element = MthElement.getMthElement(list, 2);
		assertThat(element, is("g"));
	}

}
