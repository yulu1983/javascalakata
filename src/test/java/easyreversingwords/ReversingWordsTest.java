package easyreversingwords;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class ReversingWordsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void wordShouldBeReversed1() {
		
		String reversed = ReversingWords.reverse("Hello World");
		assertThat(reversed, is("World Hello"));	
	}
	
	@Test
	public void wordShouldBeReversed2() {
		String reversed = ReversingWords.reverse("Hello CodeEval");
		assertThat(reversed, is("CodeEval Hello"));
	}
	
	@Test
	public void singleWordShouldReturnSingleWord() {
		String reversed = ReversingWords.reverse("Hello");
		assertThat(reversed, is("Hello"));
	}

}
