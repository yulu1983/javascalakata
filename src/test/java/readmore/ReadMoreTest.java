package readmore;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class ReadMoreTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenLengthIsLessThanOrEqual55TextShouldNotBeTrimmed() {
		assertThat(ReadMore.trim("Tom exhibited."), is(equalTo("Tom exhibited.")));
		assertThat(ReadMore.trim("Tom was tugging at a button-hole and looking sheepish."), is(equalTo("Tom was tugging at a button-hole and looking sheepish.")));
	}
	
	
	@Test
	public void whenLengthIsGreaterThan55TextShouldBeTrimmed1() {
		assertThat(ReadMore.trim("Amy Lawrence was proud and glad, and she tried to make Tom see it in her face - but he wouldn't look."), 
				is(equalTo("Amy Lawrence was proud and glad, and she... <Read More>")));
	}
	
	@Test
	public void whenLengthIsGreaterThan55TextShouldBeTrimmed2() {
		assertThat(ReadMore.trim("Two thousand verses is a great many - very, very great many."), 
				is(equalTo("Two thousand verses is a great many -... <Read More>")));
	}
	
	@Test
	public void whenLengthIsGreaterThan55TextShouldBeTrimmed3() {
		assertThat(ReadMore.trim("Tom's mouth watered for the apple, but he stuck to his work."), 
				is(equalTo("Tom's mouth watered for the apple, but... <Read More>")));
	}
	
	@Test
	public void test() {
		assertThat(ReadMore.trim("123456789A123456789B123456789C123456789D1 3456789E1234 6"), 
				is(equalTo("123456789A123456789B123456789C123456789D... <Read More>")));
	}
	
	

}
