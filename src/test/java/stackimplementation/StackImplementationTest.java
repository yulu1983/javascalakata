package stackimplementation;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class StackImplementationTest {

	private StackImplementation st;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		st = new StackImplementation();
		st.push(1);
		st.push(2);
		st.push(3);
		st.push(4);	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void stackImplementationShouldPushIntegerOntoStack() {	
		assertThat(st.toString(), is(equalTo("[1, 2, 3, 4]")));
	}
	
	@Test
	public void stackImplementationShouldPopIntegerFromStack() {
		Integer i1 = st.pop();
		assertThat(st.size(), is(equalTo(3)));
		assertThat(i1, is(equalTo(4)));
	}
	
	@Test(expected=EmptyStackException.class) 
	public void popEmptyStackShouldReturnEmptyStackException() {
		int s = st.size();
		for(int i = 0; i <= s; ++i) {
		   st.pop();
		}
	}
}
