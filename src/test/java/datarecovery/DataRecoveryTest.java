package datarecovery;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class DataRecoveryTest {

	@Test
	public void test1() {
		String s = DataRecovery.recovery("2000 and was not However, implemented 1998 it until;9 8 3 4 1 5 7 2");
	    assertThat(s, equalTo("However, it was not implemented until 1998 and 2000"));
	}
	
	@Test
	public void test2() {
		String s = DataRecovery.recovery("programming first The language;3 2 1");
	    assertThat(s, equalTo("The first programming language"));
	}
	
	@Test
	public void test3() {
		String s = DataRecovery.recovery("programs Manchester The written ran Mark 1952 1 in Autocode from;6 2 1 7 5 3 11 4 8 9");
	    assertThat(s, equalTo("The Manchester Mark 1 ran programs written in Autocode from 1952"));
	}

}
