package wordwrap;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class WordWrapIterativeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void whenEmptyStringShouldReturnEmptyString() {
		String s = WordWrapper.wrapIterative("", 10);
		assertThat(s, is(equalTo("")));
	}
	
	@Test
	public void whenColumnLengthGreaterThanStringShouldReturnString() {
		String s = WordWrapper.wrapIterative("hello", 10);
		assertThat(s, is(equalTo("hello")));
	}
	
	@Test
	public void whenColumnLengthLessThanStringShouldWrap() {
		String s = WordWrapper.wrapIterative("wordwordword", 4);
		assertThat(s, is(equalTo("word\nword\nword")));
	}
	
	@Test
	public void whenColumnLengthOnSpaceThanSpaceShouldBeReplacedWithNewLine() {
		String s = WordWrapper.wrapIterative("word word word", 4);
		assertThat(s, is((equalTo("word\nword\nword"))));
	}
	
	@Test
	public void whenColumnLengthGreaterThanFirstTwoWordsShouldReplace2ndSpaceIntoNewLine() {
		String s = WordWrapper.wrapIterative("word word word", 12);
		assertThat(s, is(equalTo("word word\nword")));
	}

}
