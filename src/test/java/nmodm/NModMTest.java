package nmodm;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class NModMTest {

	@Test
	public void whenBothNAndMArePositiveAndNIsBiggerModShouldReturnTheCorrectMod() {
		int mod = NModM.mod(20, 6);
		assertThat(mod, equalTo(2));
	}
	
	@Test
	public void whenBothNAndMArePositiveAndNIsEqualToMModShouldReturnZero() {
		int mod = NModM.mod(20, 20);
		assertThat(mod, equalTo(0));
	}
	
	@Test
	public void whenBothNAndMArePositiveAndNIsLessThanMModShouldReturnN() {
	   int mod = NModM.mod(20, 21);
	   assertThat(mod, equalTo(20));
	}
}
