package mergesort;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MergeSortTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void mergeSortTest() {
		Integer[] array = {49, 30, 24, 48, 55, 67, 45, 34, 22, 11, 100};
		
		MergeSort.sort(array);
		
		for(Integer i : array)
		   System.out.println(i);
		
		assertArrayEquals(array, new Integer[] {11, 22, 24, 30, 34, 45, 48, 49, 55, 67, 100});
	}

}
