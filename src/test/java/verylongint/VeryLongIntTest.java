package verylongint;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class VeryLongIntTest {

	protected VeryLongInt very;
	protected String answer;
	
	@Before
	public void setUp() throws Exception {
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test (expected = IllegalArgumentException.class)
	public void whenConstructorArgContainsNoDigitIllegalArgumentExceptionShouldBeThrown() {
	   very = new VeryLongInt("x t>.o");
	   // constructor should throw IllegalArgumentException
	}
	
	@Test
	public void addShouldProduceCorrectResult() {
		very = new VeryLongInt("99");
		VeryLongInt other = new VeryLongInt("123");
		very.add(other);
		answer = very.toString();
		assertThat(answer, is(equalTo("[2, 2, 2]")));
	}

}
