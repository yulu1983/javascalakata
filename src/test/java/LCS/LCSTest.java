package LCS;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class LCSTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void LCSTest1() {
	    String s = LCS.find("XMJYAUZ", "MZJAWXU");
		assertThat(s, is(equalTo("MJAU")));
		//assertThat(length, is(equalTo(4)));
	}

	@Test
	public void LCSTest2() {
		String s = LCS.find("BDCAB", "ABCB");
		assertThat(s, is(equalTo("BCB")));
	}
	
	@Test
	public void LCSTest3() {
		String s = LCS.find("ABCDCBA", "DCBAABC");
		assertThat(s, is(equalTo("DCBA")));
	}

}
