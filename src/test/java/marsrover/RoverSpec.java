package marsrover;

import static org.junit.Assert.*;
import marsrover.Rover.Direction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class RoverSpec {

	private Rover rover;
	private Point point;
	
	@Before
	public void setUp() throws Exception {
		point = new Point(0, 0);
		rover = new Rover(point, Direction.NORTH);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void roverShouldForward() {
	   rover.receiveCommand("F");
	   assertThat(rover.getX(), is(equalTo(0)));
	   assertThat(rover.getY(), is(equalTo(1)));
	   
	}
	
	@Test
	public void roverShouldMoveBackward() {
		rover.receiveCommand("B");
		assertThat(rover.getX(), is(equalTo(0)));
		assertThat(rover.getY(), is(equalTo(-1)));
	}
	
	@Test
	public void roverShouldTurnLeft() {
		rover.receiveCommand("L");
		assertThat(rover.getDirection(), is(equalTo(Direction.WEST)));
	}
	
	@Test
	public void roverShouldTurnRight() {
		rover.receiveCommand("R");
		assertThat(rover.getDirection(), is(equalTo(Direction.EAST)));
	}
	
	@Test
	public void roverShouldMoveNorthTwice() {
		rover.receiveCommand("FF");
		assertThat(rover.getY(), is(equalTo(2)));
		assertThat(rover.getX(), is(equalTo(0)));
	}

}
