package prefixexpression;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;

public class PrefixExpressionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void prefixExpressionTest1() {
		int i = PrefixExpression.eval("* + 2 3 4");
		assertThat(i, is(equalTo(20)));
	}

}
